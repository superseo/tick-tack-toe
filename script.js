const tr = {
    gameOver: 'Игра окончена',
    isUserWin: 'Вы победили!',
    isEnemyWin: 'Вы проиграли',
    isDraw: 'Ничья'
};

const getRandom = (min = 0, max = 1) => min === max ? max : Math.round(Math.random() * (max - min) + min);

class Game {
    constructor([alertDom, boxesDom, tableSizeDom, winLengthDom]) {
        this._alertDom = alertDom;
        this._boxesDom = boxesDom;
        this._tableSizeDom = tableSizeDom;
        this._winLengthDom = winLengthDom;
        this.tableSize = +tableSizeDom.value;
        this.winLength = +winLengthDom.value;
        this.init();
    }
    get isGameOver() {
        return this.enemy.gameOver;
    }
    get boxesNodeList() {
        if (this._boxesNodeList && this._boxesNodeList.length) {
            return this._boxesNodeList;
        }
        for (let i = 0; i < this.tableSize; i++) {
            let row = document.createElement('div');
            row.className = 'row';
            for (let i = 0; i < this.tableSize; i++) {
                let box = document.createElement('div');
                box.className = 'box';
                row.appendChild(box);
                (this._boxesNodeList = this._boxesNodeList || []).push(box);
            }
            this._boxesDom.appendChild(row);
        }
        return this._boxesNodeList || [];
    }
    init() {
        this.boxes = this.initBoxes();
        this.enemy = new Enemy();
        this.reset(1);
        this._tableSizeDom.onchange = (e) => {
            this.changeParams(e.target.value, 'table');
        }
        this._winLengthDom.onchange = (e) => {
            this.changeParams(e.target.value, 'win');
        }
    }
    initBoxes() {
        this._boxesDom.innerHTML = '';
        this._boxesNodeList = [];
        return [...this.boxesNodeList].map(this.initBox.bind(this));
    }
    initBox(boxDom, index) {
        // todo вынести new Box
        let box = new Box(boxDom, index);
        boxDom.onclick = this.userStep.bind(this, box);
        return box;
    }
    userStep(box) {
        if (!box.status && !this.isGameOver) {
            box.status = this.gamerStatus;
            this.enemy.checkStep();
        }
        if (this.isGameOver) {
            this._alertDom.innerHTML = this.enemy.getResult();
        }
    }
    changeParams(value, type) {
        if (type == 'table' && value !== this.tableSize) {
            this.tableSize = +value;
            this._winLengthDom.max = +value;
        }
        if (type == 'win' && value !== this.winLength) {
            this.winLength = +value;
        }
        this.boxes = this.initBoxes();
        this.reset();
    }
    // todo flag - восстановить игру из localStorage
    reset(flag) {
        this.gamerStatus = getRandom(1, 2);
        this.boxes.forEach(box => {
            box.status = flag ? box.status : '';
            box._dom.classList.remove('green', 'red');
        });
        this._alertDom.innerHTML = '';
        this.enemy.init(this.boxes, this.gamerStatus, this.tableSize, this.winLength);
    }
}

// Proxy управляющий localStorage
class ExtendableProxy {
    constructor() {
        return new Proxy(this, {
            get: (target, property) => {
                let value;
                if (property === 'status' && target._index !== undefined && localStorage.getItem(`boxes_${target._index}`) !== 'undefined') {
                    value = JSON.parse(localStorage.getItem(`boxes_${target._index}`));
                    return value;
                }
                return target[property];
            },
            set: (target, property, value) => {
                if (property === 'status' && target._index !== undefined) {
                    var serialObj = JSON.stringify(+value);
                    localStorage.setItem(`boxes_${target._index}`, serialObj);
                }
                target[property] = value;
                return true;
            }
        });
    }
}

class Box extends ExtendableProxy {
    constructor(_dom = {}, index = null) {
        super();
        this._dom = _dom;
        this._index = index;
    }
    get status() {
        return this._dom.getAttribute('status');
    }
    set status(status = '') {
        this._dom.setAttribute('status', status);
    }
}

class Enemy {
    constructor() {
    }
    init(boxes, gamerStatus, tableSize, winLength) {
        this.gameOver = false;
        this.isUserWin = false;
        this.isEnemyWin = false;
        this.isDraw = false;
        this._boxes = boxes;
        this.winBoxes = Combinations.winBoxes(boxes, tableSize, winLength);
        this.gamerStatus = gamerStatus;
        this.enemyStatus = this.gamerStatus == 1 ? 2 : 1;
    }
    checkStep() {
        return this.gameOver = this.gameOver || this.enemyStep();
    }
    // определяем победу игрока и возможность следующего хода
    enemyStep() {
        let combinations = []; // проверенные комбинации
        let empties = [], gamerNeeds = []; // все пустые ячейки
        let bestChoices = []; // лучшие ячейки
        let {_boxes, isUserWin, isEnemyWin, isDraw, gamerStatus, enemyStatus, resultCombination} = this;
        if (isUserWin || isEnemyWin || isDraw) {
            return true;
        }

        // проход по комбинациям в один цикл
        this.winBoxes.some((winCombination) => {
            let combination = Combinations.checkCombination(winCombination, [gamerStatus, enemyStatus]);
            combinations.push(combination);
            let enemyNeed = combination[enemyStatus].need;
            let gamerNeed = combination[gamerStatus].need;
            let gamerDone = combination[gamerStatus].done;
            if (gamerDone && gamerDone.length) {
                gamerNeeds.push(...gamerNeed);
            }
            empties.push(...gamerNeed, ...enemyNeed);
        });
        bestChoices = Combinations.getBestChoices(gamerNeeds && gamerNeeds.length ? gamerNeeds : empties);

        let next;
        combinations.some(combination => {
            let len = combination.checked.length;
            let gamerNeed = combination[gamerStatus].need;
            let enemyNeed = combination[enemyStatus].need;
            let enemyBusy = combination[enemyStatus].busy;

            isUserWin = isUserWin || len === combination[gamerStatus].done.length;
            isEnemyWin = isEnemyWin || len === combination[enemyStatus].done.length;

            if (isUserWin || isEnemyWin) {
                if (isUserWin && !isEnemyWin) {
                    resultCombination = combination.checked;
                }
                return true;
            }

            // 2. может ли компьютер победить следующим ходом
            if (enemyNeed.length === 1) {
                next = enemyNeed[getRandom(0, enemyNeed.length - 1)];
                isEnemyWin = !enemyBusy.length;
            // 3. либо не даем выиграть
            } else if (gamerNeed.length === 1) {
                next = gamerNeed[getRandom(0, gamerNeed.length - 1)];
            }
            if (isEnemyWin) {
                resultCombination = combination.checked;
            }
        });

        // ничья
        isDraw = !bestChoices.length;
        if (!isUserWin && !isDraw) {
            // ход в конце цикла
            if (next == undefined && bestChoices.length) {
                next = bestChoices[getRandom(0, bestChoices.length - 1)];
            }

            if (next !== undefined) {
                _boxes[next].status = enemyStatus;
            }
        }

        this.isUserWin = isUserWin;
        this.isEnemyWin = isEnemyWin;
        this.isDraw = isDraw;
        this.resultCombination = resultCombination;
        return isUserWin || isEnemyWin || isDraw;
    }
    getResult() {
        let {_boxes, gameOver, isUserWin, isEnemyWin, isDraw, resultCombination} = this;
        let message = '';
        let status = ({gameOver, isUserWin, isEnemyWin, isDraw});
        if (isUserWin || isEnemyWin) {
            resultCombination.forEach(index => {
                _boxes[index]._dom.classList.add(isUserWin ? 'green' : 'red');
            });
        }
        for (let item in status) {
            if (status[item]) message += tr[item] + '<br />';
        };
        return message;
    }
}

// выигрышными комбинациями будет управлять отдельный класс
class Combinations {
    // todo игра гомоку
    static winArray(tableSize, winLength) {
        let arr = [], dArr = [];
        let el = 0;
        let last = tableSize - winLength + 1;
        for (let row = 0; row < tableSize; row++) {
            let colArr = [], rowArr = [];
            let colArrTemp = [], rowArrTemp = [], dArrTemp1 = [], dArrTemp2 = [];
            for (let col = 0; col < tableSize; col++, el++) {
                if (winLength === tableSize) { // упрощенный расчет горизонтали, вертикали и главной и побочной диагоналей
                    let vv = col * tableSize + row;
                    rowArr.push(el);
                    colArr.push(vv);
                    if (row === 0) {
                        (dArr[0] = dArr[0] || []).push(el + vv);
                    }
                    if (row === tableSize - 1) {
                        (dArr[1] = dArr[1] || []).push(vv - col);
                    }
                } else { // либо считаем все возможные варианты
                    for (let d = 0; d < winLength; d++) {
                        if (col >= last && row >= last) {
                            continue;
                        }
                        let underCurrent = col + (d + row) * tableSize;
                        if (col < last) {
                            rowArrTemp[col] = rowArrTemp[col] || [];
                            rowArrTemp[col].push(el + d);
                        }
                        if (row < last) {
                            colArrTemp[col] = colArrTemp[col] || [];
                            colArrTemp[col].push(underCurrent);
                        }
                        if (col < last && row < last) {
                            // правая диагональ
                            dArrTemp1[col] = dArrTemp1[col] || [];
                            dArrTemp1[col].push(underCurrent + d);
                            // левая диагональ
                            dArrTemp2[col] = dArrTemp2[col] || [];
                            dArrTemp2[col].push(underCurrent - d + winLength - 1);
                        }
                    }
                    if (col === tableSize - 1) {
                        if (rowArrTemp.length) rowArr.push(...rowArrTemp);
                        if (colArrTemp.length) colArr.push(...colArrTemp);
                        if (dArrTemp1.length) dArr.push(...dArrTemp1);
                        if (dArrTemp2.length) dArr.push(...dArrTemp2);
                    }
                }
            }
            if (winLength === tableSize) {
                arr.push(colArr, rowArr);
            } else {
                arr.push(...colArr, ...rowArr);
            }
            if (row === tableSize - 1) {
                dArr.forEach(d => {
                    if (d.length === winLength) {
                        arr.push(d);
                    }
                });
            }
        }
        return arr;
    }
    static winBoxes(_boxes = [], tableSize, winLength) {
        return this.winArray(tableSize, winLength).map(combination => {
            return combination.map(i => _boxes[i]);
        });
    }
    // проверка статусов полей и выдача доступных комбинаций каждому игроку
    static checkCombination(winCombination = [], findBoxStatuses = []) {
        return winCombination.reduce((out, box) => {
            findBoxStatuses.forEach(status => {
                let key = '';
                out[status] = out[status] || {};
                out[status].done = out[status].done || [];
                out[status].busy = out[status].busy || [];
                out[status].need = out[status].need || [];
                // занято этим игроком
                if (box.status == status) {
                    key = 'done';
                // занято другим игроком
                } else if (box.status && box.status != status) {
                    key = 'busy';
                // осталось занять
                } else {
                    key = 'need';
                }
                out[status][key].push(box._index);
                if (out[status].busy && out[status].busy.length &&
                    out[status].need && out[status].need.length) {
                    // если занято игроком, то нас не интересуют
                    out[status].need = [];
                }
            });
            out.checked = winCombination.map(box => box._index);
            return out;
        }, {});
    }
    // выдача приоритетных полей для следующего хода
    static getBestChoices(array = []) {
        // суммирование одинаковых полей, получаем вес каждой клетки
        let bestChoices = array.reduce((out, value) => {
            if (value !== undefined) {
                out[value] = out[value] || {index: value};
                out[value].value = (out[value].value || 0) + 1;
            }
            return out;
        }, []);
        // максимальные значения веса и выкидывание служебной информации
        let max = 0;
        bestChoices = bestChoices.reduce((out, item) => {
            if (item.value > max) {
                max = item.value;
                out = [];
            }
            if (item.value === max) {
                out.push(item.index);
            }
            return out;
        }, []);
        return bestChoices;
    }
}